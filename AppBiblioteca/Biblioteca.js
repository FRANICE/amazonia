import { Bibliotecario } from "./controladores/Bibliotecario.js"
import { Libro } from "./controladores/Libro.js"
import { Usuario } from "./controladores/Usuario.js"
import { Utils } from "./utils.js"
import { Transaccion } from "./controladores/Transaccion.js"

export class Biblioteca {
    libros = []
    usuarios = []
    bibliotecarios = []
    transacciones = []

    constructor(nombre, direccion, codigoPostal, pais) {
        this.id = Utils.getRandomID(),
        this.nombre = nombre,
        this.direccion = direccion,
        this.codigoPostal = codigoPostal,
        this.pais = pais
    }

    /*---------------------------------------------------------Libro-------------------------------------------------------*/

    agregarLibro(id, titulo, autor, editorial, fechaPrimeraEdicion, prestado, enVenta) {
        let nuevoLibro = new Libro(id, titulo, autor, editorial, fechaPrimeraEdicion, prestado, enVenta)
        this.libros.push(nuevoLibro)
    }

    getLibroById(id) {
        return this.libros.filter(function(libro) {
            return libro.id == id; 
        })[0]
    }

    eliminarLibroById(id) {
        this.libros = this.libros.filter(function(libro) {
            return libro.id !== id; 
        });
    }

    /*----------------------------------------------------Bibliotecario-----------------------------------------------------*/

    agregarBibliotecario(id, nombre, primerApellido, segundoApellido, fechaAlta, fechaBaja) {
        const nuevoBibliotecario = new Bibliotecario(id, nombre, primerApellido, segundoApellido, fechaAlta, fechaBaja)
        this.bibliotecarios.push(nuevoBibliotecario)
    }

    getBibliotecarioById(id) {
        return this.bibliotecarios.filter(function(bibliotecario) {
            return bibliotecario.id == id; 
        })[0]
    }


    eliminarBibliotecarioById(id) {
        this.bibliotecarios = this.bibliotecarios.filter(function(bibliotecario) {
            return bibliotecario.id !== id; 
        });
    }

    /*-------------------------------------------------------Usuario---------------------------------------------------------*/

    agregarUsuario(id, nombre, primerApellido, segundoApellido, fechaAlta, fechaBaja) {
        const nuevoUsuario = new Usuario(id, nombre, primerApellido, segundoApellido, fechaAlta, fechaBaja)
        this.usuarios.push(nuevoUsuario)
    }

    getUsuarioById(id) {
        return this.usuarios.filter(function(usuario) {
            return usuario.id == id; 
        })[0]
    }

    eliminarUsuarioById(id) {
        this.usuarios = this.usuarios.filter(function(usuario) {
            return usuario.id !== id; 
        });
    }

    /*-----------------------------------------------------Transacción--------------------------------------------------------*/

    agregarTransaccion(id, idLibroPrestado, idUsuarioPrestamo, idBibliotecarioFirma, fechaTransaccion, tipoPrestamo) {
        let nuevaTransaccion = new Transaccion(id, idLibroPrestado, idUsuarioPrestamo, idBibliotecarioFirma, fechaTransaccion, tipoPrestamo)
        this.transacciones.push(nuevaTransaccion)
    }

    getTransaccionById(id) {
        return this.transacciones.filter(function(transaccion) {
            return transaccion.id == id; 
        })[0]
    }

    getTransaccionPrestado(tipoPrestamo) {
        return this.transacciones.filter(function(transaccion) {
            return transaccion.tipoPrestamo == tipoPrestamo; 
        })[0]
    }

    eliminarTransaccionById(id) {
        this.transacciones = this.transacciones.filter(function(transaccion) {
            return transaccion.id !== id; 
        });
    }

    /*---------------------------------------------------------Prestar--------------------------------------------------------*/

    prestarLibro(idBibliotecarioFirma, idUsuarioPrestamo, idLibroPrestado, tipoPrestamo) {
        let usuario = this.getUsuarioById(idUsuarioPrestamo)
        let libro = this.getLibroById(idLibroPrestado)
        let bibliotecario = this.getBibliotecarioById(idBibliotecarioFirma)
        let fechaPrestamo = new Date()
        if (!libro.prestado) {
            if (bibliotecario.prestarLibro(usuario, libro, fechaPrestamo, tipoPrestamo)) {
                this.agregarTransaccion(null, idLibroPrestado, idUsuarioPrestamo, idBibliotecarioFirma, fechaPrestamo, tipoPrestamo)
            }
        }
    }

    /*---------------------------------------------------------Devolver--------------------------------------------------------*/

    devolverLibro(idBibliotecarioFirma, idUsuarioPrestamo, idLibroPrestado) {
        let usuario = this.getUsuarioById(idUsuarioPrestamo)
        let libro = this.getLibroById(idLibroPrestado)
        let bibliotecario = this.getBibliotecarioById(idBibliotecarioFirma)
        if (libro.prestado) {
            if (bibliotecario.devolverLibro(usuario, libro)) {
                let transaccionesActivasDelLibro = this.transacciones.filter(function(transaccion) {
                    return transaccion.estaPendiente && transaccion.idLibroPrestado == idLibroPrestado; 
                });
                transaccionesActivasDelLibro[0].estaPendiente = false
            }
        }
    }

    /*----------------------------------------------------Obtener Préstamos---------------------------------------------------*/

    getTotalPrestamos() {
        let transaccionesActivas = this.transacciones.filter(function(transaccion) {
            return transaccion.estaPendiente; 
        });
        return transaccionesActivas.length
    }

    /*--------------------------------------------------Avisos para el usuario-------------------------------------------------*/

    mensajearAlUsuario() {
        for (let i = 0; i < this.usuarios.length; i++) {
            let usuario = this.usuarios[i]
            for (let j = 0; j < usuario.librosPrestados.length; j++) {
                let libroPrestado = usuario.librosPrestados[j]
                if (libroPrestado.haExpiradoPrestamo()) {
                    console.log(`Recordatorio para ${usuario.nombre}. El préstamo de tu libro, (${libroPrestado.titulo}), ha expirado. Devuélvelo a la mayor brevedad posible para evitar sanciones.`)
                }
            }
        }
    }

    /*-------------------------------------------Listar Libros con préstamo expirado--------------------------------------------*/

    listarLibrosExpirados() {
        let librosExpirados = this.libros.filter(function(libro) {
            return libro.haExpiradoPrestamo(); 
        });
        console.log(librosExpirados);

    }

}

    /*----------------------------------------------Instanciación de la biblioteca---------------------------------------------*/

const biblioteca = new Biblioteca(
    'Amazonia',
    'Calle de la Esperanza, 2',
    '15178',
    'Espanha'
)

    /*----------------------------------------------Introducción de datos/pruebas---------------------------------------------*/

biblioteca.agregarLibro(1, '1984', 'George Orwell', 'Secker & Warburg', Date.parse('1949-04-08'), false, true)
biblioteca.agregarLibro(2, 'Voces de Chernóbil', 'Svetlana Aleksiévich', 'Ostozhye', Date.parse('1997-01-01'), false, true)
biblioteca.agregarLibro(3, 'La Metamorfosis', 'Franz Kafka', 'Kurt Wolff', Date.parse('1915-01-01'), false, true)
biblioteca.agregarLibro(4, 'Crónica de una muerte anunciada', 'Gabriel García Márquez', 'La Oveja Negra', Date.parse('1981-01-01'), false, true)
biblioteca.agregarLibro(5, 'Rimas', 'Gustavo Adolfo Bécquer', 'GAB', Date.parse('1867-01-01'), false, true)
biblioteca.agregarLibro(6, '1984', 'Arturo Pérez Reverte', 'Alfaguara', Date.parse('2021-01-01'), false, true)

biblioteca.agregarBibliotecario(1, 'Fulgencio', 'Ramírez', 'Rodríguez', Date.parse('1949-04-08'), Date.parse('1949-04-08'))
biblioteca.agregarBibliotecario(2, 'Hermenegildo', 'Gómez', 'López', Date.parse('1949-04-08'), Date.parse('1949-04-08'))

biblioteca.agregarUsuario(1, 'Adrián', 'del Castillo', 'Pérez', Date.parse('1949-04-08'), Date.parse('1949-04-08'))
biblioteca.agregarUsuario(2, 'Laura', 'Gil', 'de la Peña', Date.parse('1949-04-08'), Date.parse('1949-04-08'))





/*Pruebas libros. Descomentar todas las líneas para testear.*/


// console.log("")
// console.log("Listamos todos los libros.")
// console.log("")

// console.log(biblioteca.libros)

// console.log("")
// console.log("Listamos el libro con id 4.")
// console.log("")

// console.log(biblioteca.getLibroById(4))

// console.log("")
// console.log("Modificamos el libro con id 4.")
// console.log("")

// biblioteca.getLibroById(4).modificarLibro('Jjajaajajaj', null, null, null, null, null)

// console.log("")
// console.log("Volvemos a listar el libro con id 4.")
// console.log("")

// console.log(biblioteca.getLibroById(4))

// console.log("")
// console.log("Eliminamos el libro con id 4.")
// console.log("")

// biblioteca.eliminarLibroById(4)

// console.log("")
// console.log("Volvemos a listar todos los libros.")
// console.log("")

// console.log(biblioteca.libros)





/*Pruebas usuarios. Descomentar todas las líneas para testear.*/


// console.log("")
// console.log("Listamos todos los usuarios.")
// console.log("")

// console.log(biblioteca.usuarios)

// console.log("")
// console.log("Listamos el usuario con id 2.")
// console.log("")

// console.log(biblioteca.getUsuarioById(2))

// console.log("")
// console.log("Modificamos el usuario con id 2.")
// console.log("")

// biblioteca.getUsuarioById(2).modificarUsuario('Nombre', 'Apellido1', 'Apellido2', null, null)

// console.log("")
// console.log("Volvemos a listar el usuario con id 2.")
// console.log("")

// console.log(biblioteca.getUsuarioById(2))

// console.log("")
// console.log("Eliminamos el usuario con id 2.")
// console.log("")

// biblioteca.eliminarUsuarioById(2)

// console.log("")
// console.log("Volvemos a listar todos los usuarios.")
// console.log("")

// console.log(biblioteca.usuarios)





/*Pruebas bibliotecarios. Descomentar todas las líneas para testear.*/


// console.log("")
// console.log("Listamos todos los bibliotecarios.")
// console.log("")

// console.log(biblioteca.bibliotecarios)

// console.log("")
// console.log("Listamos el bibliotecario con id 1.")
// console.log("")

// console.log(biblioteca.getBibliotecarioById(1))

// console.log("")
// console.log("Modificamos el bibliotecario con id 1.")
// console.log("")

// biblioteca.getBibliotecarioById(1).modificarBibliotecario('Nombre', 'Apellido1', 'Apellido2', null, null)

// console.log("")
// console.log("Volvemos a listar el bibliotecario con id 1.")
// console.log("")

// console.log(biblioteca.getBibliotecarioById(1))

// console.log("")
// console.log("Eliminamos el bibliotecario con id 1.")
// console.log("")

// biblioteca.eliminarBibliotecarioById(1)

// console.log("")
// console.log("Volvemos a listar todos los bibliotecarios.")
// console.log("")

// console.log(biblioteca.bibliotecarios)





/*Préstamos, devoluciones, expiraciones...*/


// console.log("")
// console.log("Listamos el libro con id 1.")
// console.log("")

// console.log(biblioteca.getLibroById(1))

// console.log("")
// console.log("Obtenemos el total de préstamos.")
// console.log("")

// console.log(biblioteca.getTotalPrestamos())

// console.log("")
// console.log("Intentamos prestar el libro con id 1 a dos usuarios diferentes.")
// console.log("")

// biblioteca.prestarLibro(1, 1, 1, 14)
// biblioteca.prestarLibro(1, 2, 1, 7)

// console.log("")
// console.log("Volvemos a obtener el total de préstamos.")
// console.log("")

// console.log(biblioteca.getTotalPrestamos())

// console.log("")
// console.log("Volvemos a listar el libro con id 1.")
// console.log("")

// console.log(biblioteca.getLibroById(1))

// console.log("")
// console.log("Listamos ambos usuarios.")
// console.log("")

// console.log(biblioteca.getUsuarioById(1))

// console.log(biblioteca.getUsuarioById(2))

// console.log("")
// console.log("Forzamos la expiración del préstamo.")
// console.log("")

// biblioteca.getLibroById(1).fechaPrestado.setDate(biblioteca.getLibroById(1).fechaPrestado.getDate() - 20)

// console.log("")
// console.log("Listamos el usuario con id 1.")
// console.log("")

// console.log(biblioteca.getUsuarioById(1))

// console.log("")
// console.log("Listamos los libros con el préstamo expirado.")
// console.log("")

// biblioteca.listarLibrosExpirados()

// console.log("")
// console.log("Enviamos mensaje al usuario notificándole acerca de la expiración del préstamo.")
// console.log("")

// biblioteca.mensajearAlUsuario()

// console.log("")
// console.log("")
// console.log("")
// console.log("")
// console.log("Devolvemos el libro prestado.")
// console.log("")

// biblioteca.devolverLibro(1, 1, 1)

// console.log("")
// console.log("Volvemos a obtener el total de préstamos.")
// console.log("")

// console.log(biblioteca.getTotalPrestamos())

// console.log("")
// console.log("Volvemos a listar el libro con id 1.")
// console.log("")

// console.log(biblioteca.getLibroById(1))

// console.log("")
// console.log("Listamos el usuario con id 1.")
// console.log("")
// console.log(biblioteca.getUsuarioById(1))

// console.log("")
// console.log("Listamos las transacciones.")
// console.log("")

// console.log(biblioteca.transacciones);

// console.log("")
// console.log("Intentamos prestar más de 5 libros a un usuario.")
// console.log("")

// biblioteca.prestarLibro(1, 2, 1, 14)
// biblioteca.prestarLibro(2, 2, 2, 7)
// biblioteca.prestarLibro(1, 2, 3, 31)
// biblioteca.prestarLibro(2, 2, 4, 7)
// biblioteca.prestarLibro(1, 2, 5, 7)
// biblioteca.prestarLibro(2, 2, 6, 14)

// console.log("")
// console.log("Listamos el usuario con id 2.")
// console.log("")

// console.log(biblioteca.getUsuarioById(2))

// console.log("")
// console.log("Comprobamos que el sexto libro no ha sido prestado.")
// console.log("")

// console.log(biblioteca.getLibroById(6));

// console.log("")
// console.log("Listamos las transacciones.")
// console.log("")

// console.log(biblioteca.transacciones);

// console.log("")
// console.log("Devolvemos varios libros.")
// console.log("")

// biblioteca.devolverLibro(2, 2, 2)
// biblioteca.devolverLibro(1, 2, 5)

// console.log("")
// console.log("Listamos las transacciones.")
// console.log("")

// console.log(biblioteca.transacciones);

// console.log("")
// console.log("Volvemos a obtener el total de préstamos.")
// console.log("")

// console.log(biblioteca.getTotalPrestamos())
