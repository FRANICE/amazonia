import { Utils } from "../utils.js"

export class Libro {
    fechaPrestado = null
    tipoPrestamo = null

    constructor(id, titulo, autor, editorial, fechaPrimeraEdicion, prestado, enVenta) {
        this.id = id || Utils.getRandomID()
        this.titulo = titulo || null,
        this.autor = autor || null,
        this.editorial = editorial || null,
        this.fechaPrimeraEdicion = fechaPrimeraEdicion || null,
        this.prestado = prestado || false,
        this.enVenta = enVenta || true
    }

    modificarLibro(titulo, autor, editorial, fechaPrimeraEdicion, prestado, enVenta) {
        this.titulo = titulo || this.titulo
        this.autor = autor || this.autor
        this.editorial = editorial || this.editorial
        this.fechaPrimeraEdicion = fechaPrimeraEdicion || this.fechaPrimeraEdicion
        this.prestado = prestado || this.prestado
        this.enVenta = enVenta || this.enVenta
    }

    // Método mediante el cual se establece el libro como prestado.
    prestar(fechaPrestamo, tipoPrestamo) {
        this.prestado = true
        this.fechaPrestado = fechaPrestamo
        this.tipoPrestamo = tipoPrestamo
    }

    // Método empleado para comprobar si un préstamo del usuario ha expirado.
    haExpiradoPrestamo() {
        if (this.fechaPrestado != null) {
            let fechaDevolucion = Utils.addDays(this.fechaPrestado, this.tipoPrestamo)
            // Bucle if inmplícito en la siguente expresión. Devuelve true o false.
            return fechaDevolucion < (new Date())
        }
        return null
    }

    // Método mediante el cual se establece el libro como no prestado .
    devolver() {
        this.prestado = false
        this.fechaPrestado = null
        this.tipoPrestamo = null
    }

}
