import { Utils } from "../utils.js";

export class Transaccion {

    estaPendiente = true
    
    constructor (id, idLibroPrestado, idUsuarioPrestamo, idBibliotecarioFirma, fechaPrestamo, tipoPrestamo) { /*Antes era fechaTransaccion*/
        this.id = id || Utils.getRandomID(),
        this.idLibroPrestado = idLibroPrestado || null,
        this.idUsuarioPrestamo = idUsuarioPrestamo || null,
        this.idBibliotecarioFirma = idBibliotecarioFirma || null,
        this.fechaPrestamo = fechaPrestamo || null,
        this.tipoPrestamo = tipoPrestamo || null
    }
    
    modificarTransaccion(id, idLibroPrestado, idUsuarioPrestamo, idBibliotecarioFirma, fechaPrestamo, tipoPrestamo) {
        this.id = id || this.id,
        this.idLibroPrestado = idLibroPrestado || this.idLibroPrestado,
        this.idUsuarioPrestamo = idUsuarioPrestamo || this.idUsuarioPrestamo,
        this.idBibliotecarioFirma = idBibliotecarioFirma || this.idBibliotecarioFirma,
        this.fechaPrestamo = fechaPrestamo || this.fechaPrestamo,
        this.tipoPrestamo = tipoPrestamo || this.tipoPrestamo
    }
}
