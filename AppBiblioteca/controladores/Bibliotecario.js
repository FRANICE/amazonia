import { Utils } from "../utils.js"

export class Bibliotecario {
    constructor (id, nombre, primerApellido, segundoApellido, fechaAlta, fechaBaja) {
        this.id = id || Utils.getRandomID(),
        this.nombre = nombre || null,
        this.primerApellido = primerApellido || null,
        this.segundoApellido = segundoApellido || null,
        this.fechaAlta = fechaAlta || null,
        this.fechaBaja = fechaBaja || null
    }

    modificarBibliotecario(nombre, primerApellido, segundoApellido, fechaAlta, fechaBaja) {
        this.nombre = nombre || this.nombre,
        this.primerApellido = primerApellido || this.primerApellido,
        this.segundoApellido = segundoApellido || this.segundoApellido,
        this.fechaAlta = fechaAlta || this.fechaAlta,
        this.fechaBaja = fechaBaja || this.fechaBaja
    }

    // Método mediante el cual se realiza el préstamo.
    prestarLibro(usuario, libro, fechaPrestamo, tipoPrestamo) {
        if (!usuario.haAlcanzadoMaximo()) {
            libro.prestar(fechaPrestamo, tipoPrestamo)
            usuario.recibirLibro(libro)
            return true
        }
        return false
    }

    // Método mediante el cual se realiza la devolución del préstamo.
    devolverLibro(usuario, libro) {
        if (usuario.tiene(libro)) {
            usuario.retirarLibro(libro)
            libro.devolver()
            return true
        }
        return false
    }
}
