import { Utils } from "../utils.js"

export class Usuario {
    librosPrestados = []

    constructor(id, nombre, primerApellido, segundoApellido, fechaAlta, fechaBaja) {
        this.id = id || Utils.getRandomID(),
        this.nombre = nombre || null,
        this.primerApellido = primerApellido || null,
        this.segundoApellido = segundoApellido || null,
        this.fechaAlta = fechaAlta || null,
        this.fechaBaja = fechaBaja || null
    }

    modificarUsuario(nombre, primerApellido, segundoApellido, fechaAlta, fechaBaja) {
        this.nombre = nombre || this.nombre,
        this.primerApellido = primerApellido || this.primerApellido,
        this.segundoApellido = segundoApellido || this.segundoApellido,
        this.fechaAlta = fechaAlta || this.fechaAlta,
        this.fechaBaja = fechaBaja || this.fechaBaja
    }

     // Método mediante el cual se comprueba si el usuario ha alcanzado el máximo
     // número de libros obtenidos mediante préstamo.
    haAlcanzadoMaximo() {
        // Bucle if inmplícito en la siguente expresión. Devuelve true o false.
        return this.librosPrestados.length == 5
    }

    // Método mediante el cual se introduce un libro en el array de libros prestados
    // al usuario.
    recibirLibro(libro) {
        this.librosPrestados.push(libro)
    }

    // ATENCIÓN: Entra el objeto libro, pero sólo cogemos el id del libro.
    // Método mediante el cual se comprueba si el usuario posee libros en préstamo o no.
    tiene({ id }) {
        let arrayConElLibroSeleccionado = this.librosPrestados.filter(function(libro) {
            // Bucle if inmplícito en la siguente expresión. Devuelve true o false.
            return libro.id == id; 
        });
        // Bucle if inmplícito en la siguente expresión. Devuelve true o false.
        return arrayConElLibroSeleccionado.length > 0
    }

    // ATENCIÓN: Entra el objeto libro, pero sólo cogemos el id del libro.
    // Método mediante el cual se retira un libro de la lista de libros del usuario.
    retirarLibro({ id }) {
        this.librosPrestados = this.librosPrestados.filter(function(libro) {
            // Bucle if inmplícito en la siguente expresión. Devuelve true o false.
            return libro.id !== id; 
        });
    }
    
}
