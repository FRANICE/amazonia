export class Utils {

    static getRandomID() {
        const min = 10000000
        const max = 99999999
        return Math.floor(Math.random() * (max - min + 1) + min)
    }

    static addDays(date, days) {
        let result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }
    
}
