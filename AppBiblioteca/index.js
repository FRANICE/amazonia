import Biblioteca from "./Biblioteca.js"
import Libro from "./controladores/Libro.js"
import Bibliotecario from "./controladores/Bibliotecario.js"
import Usuario from "./controladores/Usuario.js"

const nuevaBiblioteca = new Biblioteca(
    'Amazonia',
    'Calle de la Esperanza, 2',
    '15178',
    'Espanha'
)

const libro1 = new Libro(
    '1984',
    'George Orwell', 
    'Secker & Warburg',
    Date.parse('1949-04-08'),
    false,
    false
)

const libro2 = new Libro(
    'Voces de Chernóbil',
    'Svetlana Aleksiévich', 
    'Ostozhye',
    Date.parse('1997-01-01'),
    false,
    false
)

const libro3 = new Libro(
    'La Metamorfosis',
    'Franz Kafka', 
    'Kurt Wolff',
    Date.parse('1915-01-01'),
    false,
    false
)

const libro4 = new Libro(
    'Crónica de una muerte anunciada',
    'Gabriel García Márquez', 
    'La Oveja Negra',
    Date.parse('1981-01-01'),
    false,
    false
)

const libro5 = new Libro(
    'Rimas',
    'Gustavo Adolfo Bécquer', 
    'GAB',
    Date.parse('1867-01-01'),
    false,
    false
)

const libro6 = new Libro(
    'El Italiano',
    'Arturo Pérez Reverte', 
    'Alfaguara',
    Date.parse('2021-01-01'),
    false,
    false
)

const bibliotecario1 = new Bibliotecario(
    'Fulgencio',
    'Ramírez',
    'Rodríguez',
    Date.parse(null),
    Date.parse(null)
)

const bibliotecario2 = new Bibliotecario(
    'Hermenegildo',
    'Gómez',
    'López',
    Date.parse(null),
    Date.parse(null)
)

const usuario1 = new Usuario(
    'Adrián',
    'del Castillo',
    'Pérez',
    Date.parse(null),
    Date.parse(null)
)

const usuario2 = new Usuario(
    'Laura',
    'Gil',
    'de la Peña',
    Date.parse(null),
    Date.parse(null)
    )


nuevaBiblioteca.agregarLibro(libro1)

nuevaBiblioteca.agregarLibro(libro2)

nuevaBiblioteca.agregarLibro(libro3)

nuevaBiblioteca.agregarLibro(libro4)

nuevaBiblioteca.agregarLibro(libro5)

nuevaBiblioteca.agregarLibro(libro6)


nuevaBiblioteca.agregarBibliotecario(bibliotecario1)

nuevaBiblioteca.agregarBibliotecario(bibliotecario2)


nuevaBiblioteca.agregarUsuario(usuario1)

nuevaBiblioteca.agregarUsuario(usuario2)


console.log(nuevaBiblioteca)
